from django.shortcuts import get_object_or_404
from django.db.models import ObjectDoesNotExist
from django.http import Http404, HttpResponse
from django.shortcuts import render

import os
from test_divorce_answer.settings import  BASE_DIR

from sys import exc_info
from traceback import print_tb
from PDFlib.PDFlib import *

from random import randint

from . import models


def pdf_view(request):
    infile = (BASE_DIR + '/pdf_templates/existing_pdf_template.pdf')

    # This is where font/image/PDF input files live. Adjust as necessary.
    searchpath = "."

    left = 55
    right = 530
    fontsize = 12
    pagewidth = 595
    pageheight = 842

    p = PDFlib()

    try:
        # This means we must check return values of load_font() etc.
        p.set_option("errorpolicy=return")

        p.set_option("SearchPath={{" + searchpath + "}}")

        if p.begin_document("invoice.pdf", "") == -1:
            raise PDFlibException("Error: " + p.get_errmsg())

        p.set_info("Creator", "invoice.py")
        p.set_info("Author", "Thomas Merz")
        p.set_info("Title", "PDFlib invoice generation demo (Python)")

        stationery = p.open_pdi_document(infile, "")
        if stationery == -1:
            raise PDFlibException("Error: " + p.get_errmsg())

        page = p.open_pdi_page(stationery, 1, "")
        if page == -1:
            raise PDFlibException("Error: " + p.get_errmsg())

        boldfont = p.load_font("Helvetica-Bold", "unicode", "")
        if boldfont == -1:
            raise PDFlibException("Error: " + p.get_errmsg())
        regularfont = p.load_font("Helvetica", "unicode", "")
        if regularfont == -1:
            raise PDFlibException("Error: " + p.get_errmsg())
        leading = fontsize + 2

        # Establish coordinates with the origin in the upper left corner.
        p.begin_page_ext(pagewidth, pageheight, "topdown")

        p.fit_pdi_page(page, 0, pageheight, "")
        p.close_pdi_page(page)

        p.setfont(regularfont, fontsize)

        # fill case_number
        p.fit_textline("{}".format(randint(0, 9)), 380, 120, "position={center bottom}")
        # fill your_name id 1
        user_name = models.Answer.objects.filter(user=1, question_name__icontains="name")[0].value
        p.fit_textline("{}".format(user_name), 250, 165, "position={center bottom}")

        # fill  child_names if exist
        child_names = models.Answer.objects.filter(user=1, question_name__icontains="child_name")
        if not child_names:
            pass
        else:
            x = 180
            i = 0
            for child_name in child_names:
                p.fit_textline("{}".format(child_names[i].value), x, 260, "position={center bottom}")
                x += 110
                i += 1
        p.end_page_ext("")
        p.end_document("")
        p.close_pdi_document(stationery)
    except PDFlibException:
        print("PDFlib exception occurred:\n[%d] %s: %s" %
              ((p.get_errnum()), p.get_apiname(), p.get_errmsg()))
        print_tb(exc_info()[2])

    except Exception:
        print("Exception occurred: %s" % (exc_info()[0]))
        print_tb(exc_info()[2])

    finally:
        p.delete()

    # Create the HttpResponse object with the appropriate PDF headers.
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="invoice.pdf"'

    # Need refactor to IO
    with open("invoice.pdf", "rb") as f:
        response.write(f.read())
    os.remove("invoice.pdf")
    return response