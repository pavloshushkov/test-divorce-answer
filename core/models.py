from django.db import models

from django.conf import settings

# Create your models here.
class Answer(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='answers', blank=True, null=True)
    question_name = models.CharField(max_length=255)
    value = models.TextField()
    index = models.PositiveIntegerField(default=0) #?????